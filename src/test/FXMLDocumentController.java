/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.beans.binding.Bindings.select;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    private int row;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
       
        try {
            Connection conn=DriverManager.getConnection("jdbc:derby://localhost:1527/contact","Hong1007","l1411520");
            if(conn!=null){
                label.setText("Linked");
            }
            else
                label.setText("失敗");
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    private int doInsert() {
        int rows=0;
        String insert ="insert into TEST.COLLEAGUES values (?,?,?,?,?,?)";
         PreparedStatement stmt=null;
       try{
           stmt=conn.prepareStatement(insert);
           stmt.setInt(1,2);
           stmt.setString(2, "Test");
           stmt.setString(3, "Test");
           stmt.setString(4, "Test");
           stmt.setString(5, "Test");
           stmt.setString(6, "Test");
           stmt.setString(7, "Test");
           rows=stmt.executeUpdate();
           stmt.close();          
       }catch(SQLException ex)
       {
           Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, insert);
       }finally{
           return rows;
       }
    }
    private int doselect() throws SQLException {
        int rows=0;
        String insert ="select * from TEST.COLLEAGUES";
        try{
            String select = null;
            PreparedStatement stmt=conn.prepareStatement(select);
            ResultSet result;
            result = stmt.executeQuery();
                    while(result.next()){
                        System.out.println(result.getString(2));
                         System.out.println(result.getString("TITLE"));
                         row++;
                    }
                    result.close();
                    stmt.close();
                    
        }catch(SQLException ex){
            Logger.getLogger(FXMLDocumentController.class.getName());
        }finally{
            return rows;
        }
    }
}
